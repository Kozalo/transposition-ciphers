import pytest
from typing import List

from routecipher import util


class TestSplitStrEveryNthChars:
    def test_1(self):
        s = "123456"
        self._test_impl(['123', '456'], s, 3)

    def test_2(self):
        s = "123456"
        self._test_impl(['1234', '56'], s, 4)

    def test_empty(self):
        self._test_impl([], "", 2)

    @staticmethod
    def _test_impl(expected: List[str], *args, **kwargs):
        new_lst = util.split_str_every_nth_chars(*args, **kwargs)
        assert new_lst == expected


class TestReverseEveryNthStr:
    def test_2x2(self):
        lst = ['12', '34']
        expected = ['12', '43']
        self._test_impl(expected, lst, 2)

    def test_2x3(self):
        lst = ['12', '34', '56']
        expected = ['12', '43', '56']
        self._test_impl(expected, lst, 2)

    def test_2x4(self):
        lst = ['12', '34', '56', '78']
        expected = ['12', '43', '56', '87']
        self._test_impl(expected, lst, 2)

    def test_3x2(self):
        lst = ['123', '456']
        expected = ['321', '456']
        self._test_impl(expected, lst, 2, since=0)

    def test_empty(self):
        self._test_impl([], [], 2)

    def test_big_n(self):
        lst = ['123', '456']
        self._test_impl(lst, lst, 3)

    def test_big_n_with_since(self):
        lst = ['123', '456']
        expected = ['123', '654']
        self._test_impl(expected, lst, 3, since=1)

    def test_since(self):
        lst = ['123', '456']
        self._test_impl(lst, lst, 2, since=3)

    @staticmethod
    def _test_impl(expected: List[str], *args, **kwargs):
        new_lst = util.reverse_each_nth_str(*args, **kwargs)
        assert new_lst == expected


class TestRowsToColumns:
    def test_3x3(self):
        lst = ['123', '456', '789']
        expected = ['147', '258', '369']
        self._test_impl(expected, lst)

    def test_3x3_from_right(self):
        lst = ['123', '456', '789']
        expected = ['369', '258', '147']
        self._test_impl(expected, lst, from_right=True)

    def test_empty(self):
        self._test_impl([], [])

    @staticmethod
    def _test_impl(expected: List[str], *args, **kwargs):
        new_lst = util.rows_to_columns(*args, **kwargs)
        assert new_lst == expected

    def test_string_of_different_length(self):
        lst = ['123', '45', '67']
        with pytest.raises(ValueError):
            util.rows_to_columns(lst)


class TestAreDigitsConsecutive:
    def test_true(self):
        self._check("12345", "15342", consecutive=True)

    def test_false(self):
        self._check("1245", "51", consecutive=False)

    @staticmethod
    def _check(*cases: str, consecutive: bool):
        for key in cases:
            assert util.are_digits_consecutive([int(x) for x in key]) == consecutive
