import pytest

from routecipher.encryption import RouteCipherEncryptor, InvalidKey


TEXT_1 = "примермаршрутнойперестановки"
CIPHER_1 = "мастаеррешрноермиупвкйтрпнои"

TEXT_2 = "Hello World"
CIPHER_21 = "o   Wllo  reHld"
CIPHER_22 = "el Wld roolH"


class TestInitialization:
    def test_zero_key(self):
        with pytest.raises(InvalidKey):
            RouteCipherEncryptor(0)


class TestEncryption:
    def test_example_7(self):
        self._test_impl(7, TEXT_1, CIPHER_1)

    def test_hello_world_5(self):
        self._test_impl(5, TEXT_2, CIPHER_21)

    def test_hello_world_2(self):
        self._test_impl(2, TEXT_2, CIPHER_22)

    @staticmethod
    def _test_impl(column_count: int, text: str, expected_cipher: str):
        encryptor = RouteCipherEncryptor(column_count)
        cipher = encryptor.encrypt(text)
        assert cipher == expected_cipher


class TestDecryption:
    def test_example_7(self):
        self._test_impl(7, CIPHER_1, TEXT_1)

    def test_hello_world_5(self):
        self._test_impl(5, CIPHER_21, TEXT_2)

    def test_hello_world_2(self):
        self._test_impl(2, CIPHER_22, TEXT_2)

    @staticmethod
    def _test_impl(column_count: int, cipher: str, expected_text: str):
        encryptor = RouteCipherEncryptor(column_count)
        text = encryptor.decrypt(cipher)
        assert text == expected_text
