import pytest
from typing import Union

from routecipher.encryption import VerticalRouteCipherEncryptor, InvalidKey


TEXT_1 = "вотпримершифравертикальнойперестановки"
CIPHER_1 = "ореьекрфийа мааео тшрнсивевлрвиркпн питот "

TEXT_2 = "Hello World"
CIPHER_2 = "l r eoodHlWl"


class TestInitialization:
    def test_too_many_digits(self):
        with pytest.raises(InvalidKey):
            VerticalRouteCipherEncryptor(1234567891)

    def test_not_incremental_sequence_of_digits(self):
        with pytest.raises(InvalidKey):
            VerticalRouteCipherEncryptor(51)

    def test_zero_in_key(self):
        with pytest.raises(InvalidKey):
            VerticalRouteCipherEncryptor("1203")

    def test_empty_key(self):
        with pytest.raises(InvalidKey):
            VerticalRouteCipherEncryptor("")


class TestEncryption:
    def test_example(self):
        self._test_impl(5147263, TEXT_1, CIPHER_1)

    def test_example_str(self):
        self._test_impl("5147263", TEXT_1, CIPHER_1)

    def test_hello_world(self):
        self._test_impl(321, TEXT_2, CIPHER_2)

    @staticmethod
    def _test_impl(key: Union[int, str], text: str, expected_cipher: str):
        encryptor = VerticalRouteCipherEncryptor(key)
        cipher = encryptor.encrypt(text)
        assert cipher == expected_cipher


class TestDecryption:
    def test_example(self):
        self._test_impl(5147263, CIPHER_1, TEXT_1)

    def test_example_str(self):
        self._test_impl("5147263", CIPHER_1, TEXT_1)

    def test_hello_world(self):
        self._test_impl(321, CIPHER_2, TEXT_2)

    @staticmethod
    def _test_impl(key: Union[int, str], cipher: str, expected_text: str):
        encryptor = VerticalRouteCipherEncryptor(key)
        text = encryptor.decrypt(cipher)
        assert text == expected_text
