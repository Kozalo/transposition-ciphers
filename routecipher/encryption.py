"""
This module is responsible for encryption and decryption and consists of classes representing two methods of
doing it: `RouteCipherEncryptor` and `VerticalRouteCipherEncryptor`. It's possible to create your own implementation,
extending the `BaseTranspositionEncryptor` class, as well.
"""

from typing import List, Union

import logging
import abc
import math

from . import util


class BaseTranspositionEncryptor(abc.ABC):
    """Abstract base class defining the interface for all encryptors."""

    def __init__(self):
        """Default constructor sets the `_logger` property, that may be used by descendants for logging."""
        self._logger = logging.getLogger(self.__class__.__name__)

    @abc.abstractmethod
    def encrypt(self, text: str) -> str:
        """
        :param text: a string to encrypt
        :return: a cipher
        """
        pass

    @abc.abstractmethod
    def decrypt(self, cipher: str) -> str:
        """
        :param cipher: a previously encrypted cipher
        :return: decrypted text
        """
        pass

    @classmethod
    def _text_to_table(cls, text: str, column_count: int) -> List[str]:
        """
        Transforms a string `text` into a list of strings. Every string will be `column_count` characters long.
        If the text doesn't contain enough characters to fill the last string in the list, spaces will be appended.
        """

        lst = util.split_str_every_nth_chars(text, column_count)
        lst[-1] += (' ' * (column_count - len(lst[-1])))
        return lst

    def _log_text_table(self, rows: List[str]) -> None:
        """
        Descendants can use this method to log the "table" constructed by the `_text_to_table` method from a source
        text.

        :param rows: a list of strings where each string is a row of the table
        """

        self._logger.info("[Text table]")
        for row in rows:
            self._logger.info("|%s|", '|'.join(row))

    def _log_cipher_table(self, columns: List[str]) -> None:
        """
        Descendants can use this method to log the result "table". Typically, such tables are obtained from text tables
        by applying some kind of transposing operation to them. So rows become columns.

        :param columns: a list of strings where each string is a row of the result table that reflects a column of the
        text table
        """

        self._logger.info("[Cipher table]")
        for col in columns:
            self._logger.info("|%s|", '|'.join(col))


class InvalidKey(Exception):
    def __init__(self, message: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.message = message

    def __str__(self) -> str:
        return self.message


class RouteCipherEncryptor(BaseTranspositionEncryptor):
    """
    An encryptor that implements the following algorithm:

    **Text**: *ABCDEF*
    **Column count**: *3*

    =  =  =
    1
    =  =  =
    A  B  C
    D  E  F
    =  =  =

    =  =  =
    2
    =  =  =
    A  B  C
    F  E  D
    =  =  =

    =  =
    3
    =  =
    C  D
    B  E
    A  F
    =  =

    =  =
    4
    =  =
    C  D
    E  B
    A  F
    =  =

    **Cipher**: *CDEBAF*
    """

    def __init__(self, column_count: int):
        """
        :param column_count: a key, representing the number of columns of the table
        """

        if not column_count:
            raise InvalidKey("column_count must be within [1..)!")

        super().__init__()
        self._logger.debug("column_count is set to %d.", column_count)
        self.column_count = column_count

    def encrypt(self, text: str) -> str:
        rows = self._text_to_table(text, self.column_count)
        self._log_text_table(rows)

        columns = util.rows_to_columns(rows, from_right=True)
        columns = util.reverse_each_2nd_str(columns)

        self._log_cipher_table(columns)
        return "".join(columns)

    def decrypt(self, cipher: str) -> str:
        column_count = int(math.ceil(len(cipher) / self.column_count))
        rows = self._text_to_table(cipher, column_count)
        self._log_cipher_table(rows)

        columns = util.rows_to_columns(rows)
        columns = util.reverse_each_2nd_str(columns, since=1)
        columns = util.reverse_each_str(columns)

        self._log_text_table(columns)
        return "".join(columns).rstrip()

    @classmethod
    def _text_to_table(cls, text: str, column_count: int) -> List[str]:
        table = super()._text_to_table(text, column_count)
        return util.reverse_each_2nd_str(table)


class VerticalRouteCipherEncryptor(BaseTranspositionEncryptor):
    """
    An encryptor that implements the following algorithm:

    **Text**: *ABCDEF*

    **Key**: *213*

    =  =  =
    2  1  3
    =  =  =
    A  B  C
    D  E  F
    =  =  =

    =  =  =
    1  2  3
    =  =  =
    B  A  C
    E  D  F
    =  =  =

    ==  ==
    Result
    ======
    B   E
    A   D
    C   F
    ==  ==

    **Cipher**: *BEADCF*
    """

    def __init__(self, key: Union[int, str]):
        """
        :param key: a sequence of unique digits, representing the order of columns
        """

        key_as_list = [int(ch) for ch in str(key)]
        if not key_as_list:
            raise InvalidKey("Empty key!")
        if len(key_as_list) > 9 or 0 in key_as_list or len(set(key_as_list)) != len(key_as_list):
            raise InvalidKey("The key can be only a set of digits from 1 to 9!")
        if not util.are_digits_consecutive(key_as_list):
            raise InvalidKey("Digits in the key are not consecutive!")

        super().__init__()
        self._logger.debug("key is set to %d.", key)
        self.key = key_as_list

    def encrypt(self, text: str) -> str:
        rows = self._text_to_table(text, len(self.key))
        self._log_text_table(rows)

        keys_to_cols = {}
        for i, k in enumerate(self.key):
            line = "".join(s[i] for s in rows)
            keys_to_cols[k] = line
        self._logger.debug(keys_to_cols)

        columns = util.get_values_ordered_by_keys(keys_to_cols)

        self._log_cipher_table(columns)
        return "".join(columns)

    def decrypt(self, cipher: str) -> str:
        column_count = int(math.ceil(len(cipher) / len(self.key)))
        rows = self._text_to_table(cipher, column_count)
        self._log_cipher_table(rows)

        keys_to_rows = {i+1: rows[i] for i in range(len(rows))}
        rows_in_restored_order = [keys_to_rows[k] for k in self.key]
        columns = util.rows_to_columns(rows_in_restored_order)

        self._log_text_table(columns)
        return "".join(columns).rstrip()
