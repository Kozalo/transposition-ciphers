"""Various functions used by the `encryption` module."""


from typing import List, Generator, Optional, Sequence

import logging
import operator
import functools


_logger = logging.getLogger(__name__)


def split_str_every_nth_chars(s: str, n: int) -> List[str]:
    return [s[i:i + n] for i in range(0, len(s), n)]


# noinspection PyIncorrectDocstring
def reverse_each_nth_str(lst: List[str], n: int, since: Optional[int] = None) -> List[str]:
    """
    Reverse each `n`th string in the list `lst`.

    :param since: index of the first string to process; by default, equals to ``n-1`` (i.e.
    ``reverse_each_nth_str(['foo', 'bar', 'baz', '123', '654', '789'], 3)`` =>
    ``['foo', 'bar', 'zab', '123', '654', '987']``, but
    ``reverse_each_nth_str(['foo', 'bar', 'baz', '123', '654', '789'], 3, since=0)`` =>
    ``['oof', 'bar', 'baz', '321', '654', '789']``)
    """

    if since is None:
        since = n - 1
    _logger.debug("reverse_each_nth_str for list=%s, n=%d, since=%s", lst, n, since)

    if since >= len(lst):
        return lst

    new_lst = lst[:since]
    lst = lst[since:]
    gen = (x for x in lst)
    new_lst.append(reverse_str_from_gen(gen))
    _logger.debug("lst=%s, new_lst=%s", lst, new_lst)

    while True:
        try:
            for _ in range(n - 1):
                new_lst.append(next(gen))
            reversed_ = reverse_str_from_gen(gen)
        except StopIteration:
            return new_lst
        new_lst.append(reversed_)


# Shortcuts for frequent cases.
reverse_each_str = functools.partial(reverse_each_nth_str, n=1)
reverse_each_2nd_str = functools.partial(reverse_each_nth_str, n=2)


def reverse_str_from_gen(gen: Generator[str, None, None]) -> str:
    return "".join(reversed(next(gen)))


def rows_to_columns(lst: List[str], from_right: bool = False) -> List[str]:
    """
    :param lst: a list of strings where each string is a row of the table
    :param from_right: if true, rows will be processed from right to left, else (by default) in a usual manner from left
    to right
    :return: a transposed matrix
    """

    if len(lst) == 0:
        return []
    column_count = len(lst[0])
    if list(filter(lambda s: len(s) != column_count, lst)):
        raise ValueError("Strings of different lengths!")

    columns = []
    range_obj = range(column_count - 1, -1, -1) if from_right else range(column_count)
    for i in range_obj:
        new_col = [s[i] for s in lst]
        columns.append("".join(new_col))
    return columns


def get_values_ordered_by_keys(d: dict) -> list:
    ordered_pairs = sorted(d.items(), key=operator.itemgetter(0))
    __, values = zip(*ordered_pairs)
    return values


def are_digits_consecutive(digits: Sequence[int]) -> bool:
    """
    >>> are_digits_consecutive([1, 3, 5, 2, 4]) == True
    >>> are_digits_consecutive([5, 1]) == False
    :param digits: a sequence of numbers
    :return: true if the sequence represents consecutive numbers
    """
    digits = list(digits)
    digits.sort()
    for i in range(1, len(digits)):
        if digits[i] - digits[i-1] > 1:
            return False
    return True
