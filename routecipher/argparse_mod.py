"""Modified formatter for the argparse module to provide a way to break the description block into paragraphs separated
from each other by blank lines.

Code was taken from here: https://stackoverflow.com/a/32974697
"""

import argparse
import textwrap


class MultilineFormatter(argparse.HelpFormatter):
    def _fill_text(self, text, width, indent):
        text = self._whitespace_matcher.sub(' ', text).strip()
        paragraphs = text.split("|n ")
        multiline_text = ''
        for paragraph in paragraphs:
            formatted_paragraph = textwrap.fill(paragraph, width,
                                                initial_indent=indent, subsequent_indent=indent) + "\n\n"
            multiline_text = multiline_text + formatted_paragraph
        return multiline_text
