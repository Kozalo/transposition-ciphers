import sys
import logging
import argparse

from .encryption import RouteCipherEncryptor, VerticalRouteCipherEncryptor, InvalidKey
from .argparse_mod import MultilineFormatter
from . import __version__


def main() -> None:
    parser = argparse.ArgumentParser(
        description="""A tool to encrypt or decrypt some text using transposition ciphers.
        |n
        By default, the simplest route cipher is used. It takes a count of columns as a key and uses that key to generate
        a table by filling it from left to right in the first row, from right to left in the second row, from left to right
        again in the third row and so on. The output is constructed by the second traversal. The cells of the last column
        are read from bottom to top, the cells of the pre-last column - from top to bottom, etc.
        |n
        If you pass the "--vertical" option, another algorithm will be used. The vertical route cipher uses a key of N
        characters consisting of digits from 1 to N. Note that N must be less than 10! The N is the count of columns.
        The rows of the table are filled from left to right. After that, columns are reordered according to the digits
        in the key. Finally, all columns are read from top to bottom, and their values become the output.
        """,
        epilog="(c) Leonid Kozarin <kozalo@sadbot.ru> (Kozalo.Ru, 2018)",
        formatter_class=MultilineFormatter)
    parser.add_argument("operation", choices=['encrypt', 'decrypt'])
    parser.add_argument("--vertical", action="store_true",
                        help="if present, the vertical route transposition will be used")
    parser.add_argument("-v", action="count", default=0, dest="verbosity")
    parser.add_argument("key", type=int, help="see the description above")
    parser.add_argument("text", type=str, help="text to encrypt/decrypt")
    parser.add_argument("--version", action="version", version="%(prog)s " + __version__)
    args = parser.parse_args()

    if args.verbosity >= 2:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbosity >= 1:
        logging.basicConfig(level=logging.INFO)

    if args.vertical:
        try:
            encryptor = VerticalRouteCipherEncryptor(args.key)
        except InvalidKey as err:
            print(err, file=sys.stderr)
            sys.exit(1)
    else:
        encryptor = RouteCipherEncryptor(args.key)
    output = encryptor.encrypt(args.text) if args.operation == "encrypt" else encryptor.decrypt(args.text)
    print(output)


if __name__ == "__main__":
    main()
