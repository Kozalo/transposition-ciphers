Transposition ciphers
=====================

A program allows a user to encrypt or decrypt small messages using simple [transposition ciphers]. It was made as a
laboratory work on Information Security for NArFU.

By default, the simplest route cipher is used. It takes a count of columns as a key and uses that key to generate a
table by filling it from left to right in the first row, from right to left in the second row, from left to right again
in the third row and so on. The output is constructed by the second traversal. The cells of the last column are read
from bottom to top, the cells of the pre-last column — from top to bottom, etc.

If you pass the `--vertical` option, another algorithm will be used. The vertical route cipher uses a key of **n**
characters consisting of digits from 1 to **n**. Note that **n** must be less than 10! The **n** is the count of
columns. The rows of the table are filled from left to right. After that, columns are reordered according to the digits
in the key. Finally, all columns are read from top to bottom, and their values become the output.

[transposition ciphers]: https://en.wikipedia.org/wiki/Transposition_cipher#Route_cipher


How to install it
-----------------

First, you need **Python 3** and **pip** to be installed on your machine. Then download the latest stable version of
the package from the **Downloads** section and execute the following command:

    pip install --user <path-to-package>


How to use it
-------------

The program has a simple command line interface:

    routecipher [--vertical] [-v] {encrypt,decrypt} key text

| Argument     | Description                                                  |
| :----------: | ------------------------------------------------------------ |
| `--vertical` | was described above                                          |
| `-v`, `-vv`  | sets the level of logging messages to _INFO_ or even _DEBUG_ |
| _key_        | depends on chosen method of encryption; see examples below   |


Examples
--------

    routecipher encrypt 6 "Hello World!"
    routecipher encrypt --vertical 1324 "Hello World!"
    routecipher decrypt -vv 6 " Woolrlled!H"
