from setuptools import setup, find_packages
from os.path import join, dirname
import versioneer

setup(
    name="routecipher",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="A program allows a user to encrypt or decrypt small messages using simple transposition ciphers.",
    long_description=open(join(dirname(__file__), "README.md")).read(),
    long_description_content_type="text/markdown",
    keywords="transposition route ciphers encryption decryption",
    author="Leonid Kozarin",
    author_email="kozalo@sadbot.ru",
    url="https://bitbucket.org/Kozalo/transposition-ciphers",
    license="MIT",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Topic :: Security :: Cryptography',
        'Sentinel :: Forbid Publishing'     # wrong classifier to prevent occasional uploading
    ],
    packages=find_packages(exclude=['tests']),
    entry_points={
        'console_scripts': ['routecipher = routecipher.main:main']
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
)
